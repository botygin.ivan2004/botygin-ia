#pragma once
#include <iostream>

template <class Data>
class DictionaryList
{
private:
	struct Node
	{
		Data word_;
		Node* next_;
		Node() : word_(""), next_(nullptr) {};
	};
	Node* first_;
	Node* last_;
public:

	DictionaryList();
	~DictionaryList();
	DictionaryList(const DictionaryList& list2) = delete;
	DictionaryList& operator=(const DictionaryList& list2) = delete;
	DictionaryList(DictionaryList&& list2) noexcept;
	DictionaryList& operator=(DictionaryList&& list2) noexcept;
	void insertNode(const Data x);
	void deleteNode(const Data x);
	bool searchForNode(const Data x);
	void printList();
	void merge(DictionaryList* list2);
	void deleteWords(const DictionaryList* list2);
	friend DictionaryList<Data> getIntersection(const DictionaryList<Data>* list1, const DictionaryList<Data>* list2)
	{
		DictionaryList<Data> temp;
		Node* i = list1->first_;
		Node* j = list2->first_;

		while (i != nullptr && j != nullptr)
		{
			if (i->word_ == j->word_)
			{
//				temp.insertNode(i->word_);
				if (temp.first_ == nullptr)
				{
					temp.first_ = new Node();
					temp.first_->word_ = i->word_;
					temp.last_ = temp.first_;
				}
				else
				{
					Node* node = new Node();
					node->word_ = i->word_;
					temp.last_->next_ = node;
					temp.last_ = temp.last_->next_;
				}
				i = i->next_;
				j = j->next_;
			}
			else if (i->word_ < j->word_)
				i = i->next_;
			else
				j = j->next_;
		}
		return temp;
	}
};

template <class Data>
DictionaryList<Data>::DictionaryList() :
	first_(nullptr),
	last_(nullptr)
{}

template <class Data>
DictionaryList<Data>::~DictionaryList()
{
	while (first_)
	{
		Node* temp = first_;
		first_ = first_->next_;
		delete temp;
	}
}

template <class Data>
DictionaryList<Data>::DictionaryList(DictionaryList&& list2) noexcept :
	first_(list2.first_),
	last_(list2.last_)
{
	while (list2.first_)
	{
		Node* temp = list2.first_;
		list2.first_ = list2.first_->next_;
		delete temp;
	}
}

template <class Data>
DictionaryList<Data>& DictionaryList<Data>::operator=(DictionaryList&& list2) noexcept
{
	while (first_)
	{
		Node* temp = first_;
		first_ = first_->next_;
		delete temp;
	}
	this->first_ = list2.first_;
	this->last_ = list2.last_;
	list2.first_ = nullptr;
	list2.last_ = nullptr;
	return *this;
}

template <class Data>
void DictionaryList<Data>::insertNode(const Data x)
{
	if (first_ == nullptr)
	{
		first_ = new Node();
		first_->word_ = x;
		last_ = first_;
	}
	else
	{
		Node* previous = first_;
		if (first_->word_ == x)
			throw ("������� ��� ���� � ������");
		while (previous->next_ != nullptr && previous->next_->word_ <= x)
		{
			if (previous->next_->word_ == x)
				throw ("������� ��� ���� � ������");
			previous = previous->next_;
		}
		if (first_->word_ > x)
		{
			Node* node = new Node();
			node->word_ = x;
			node->next_ = first_;
			first_ = node;
			return;
		}
		Node* node = new Node();
		node->word_ = x;
		node->next_ = previous->next_;
		previous->next_ = node;
		if (previous == last_)
		{
			last_ = node;
		}
	}
}

template <class Data>
void DictionaryList<Data>::deleteNode(const Data x)
{
	if (first_ == nullptr)
		throw "������ ������. �� ������� ������ ������ ������� ����";
	if (first_->word_ == x)
	{
		Node* temp = first_;
		first_ = first_->next_;
		if (last_ == temp)
			last_ = nullptr;
		delete temp;
		return;
	}
	Node* previous = first_;
	Node* current = first_->next_;
	while (current && current->word_ != x)
	{
		previous = previous->next_;
		current = current->next_;
	}
	if (!current)
	{
		throw "���� ��� � ������";
	}
	previous->next_ = current->next_;
	if (last_ == current)
		last_ = previous;
	delete current;
}

template <class Data>
bool DictionaryList<Data>::searchForNode(const Data x)
{
	for (Node* temp = first_; temp != nullptr; temp = temp->next_)
	{
		if (temp->word_ == x)
			return true;
	}
	return false;
}

template <class Data>
void DictionaryList<Data>::printList()
{
	for (Node* temp = first_; temp != nullptr; temp = temp->next_)
	{
		std::cout << temp->word_ << std::endl;
	}
}

template <class Data>
void DictionaryList<Data>::merge(DictionaryList* list2)
{
	Node* start = first_;
	//	insertNode(temp->word_);
	if (list2->first_ == nullptr)
	{
		return;
	}
	if (first_ == nullptr)
	{
		first_ = list2->first_;
		last_ = list2->last_;
		list2->first_ = nullptr;
		list2->last_ = nullptr;
	}
	else
	{
		if (list2->first_->word_ < first_->word_)
		{
			Node* temp = list2->first_;
			list2->first_ = temp->next_;
			temp->next_ = start;
			first_ = temp;
		}
		while (list2->first_ != nullptr && start->next_ != nullptr)
		{
			if (start->word_ == list2->first_->word_)
			{
				Node* node = list2->first_;
				list2->first_ = list2->first_->next_;
				delete node;
			}
			else if (list2->first_->word_ > start->word_ && list2->first_->word_ < start->next_->word_)
			{
				Node* node = list2->first_;
				list2->first_ = list2->first_->next_;
				node->next_ = start->next_;
				start->next_ = node;
				start = start->next_;
			}
			else start = start->next_;
		}
		if (start->next_ == nullptr && list2->first_ != nullptr)
		{
			if (list2->first_->word_ == start->word_)
			{
				Node* node = list2->first_;
				list2->first_ = list2->first_->next_;
				delete node;
			}
			if (list2->first_ != nullptr)
			{
				start->next_ = list2->first_;
				last_ = list2->last_;
				list2->first_ = nullptr;
				list2->last_ = nullptr;
			}
		}
	}
}

template <class Data>
void DictionaryList<Data>::deleteWords(const DictionaryList* list2)
{
	Node* i = first_;
	Node* j = list2->first_;
	Node* prev = new Node;
	prev->next_ = first_;
	while (i != nullptr && j != nullptr)
	{
		if (i->word_ == j->word_)
		{
			if (i->word_ == first_->word_)
			{
				Node* temp = first_;
				first_ = first_->next_;
				if (last_ == temp)
					last_ = nullptr;
				delete temp;
				i = first_;
				prev = nullptr;
				j = j->next_;
			}
			else
			{
				Node* temp = i;
				i = temp->next_;
				prev->next_ = temp->next_;
				if (last_ == temp)
					last_ = prev;
				delete temp;
				j = j->next_;
			}
		}
		else if (j->word_ > i->word_)
		{
			prev = i;
			i = i->next_;
		}
		else
		{
			j = j->next_;
		}
	}
}

void printMenu()
{
	system("pause");
	system("cls");
	std::cout << "����" << std::endl;
	std::cout << "1. �������� �����" << std::endl;
	std::cout << "2. ������� �����" << std::endl;
	std::cout << "3. ����� �����" << std::endl;
	std::cout << "4. ������� �������" << std::endl;
	std::cout << "5. ���������� �������" << std::endl;
	std::cout << "6. ������� �������" << std::endl;
	std::cout << "7. �������� ����������� ��������" << std::endl;
	std::cout << "8. ������� ������ �������" << std::endl;

	std::cout << "����� ������ �������� - ��� ���������� ������ ���������" << std::endl;
}


bool isCorrectWord(std::string x)
{
	for (std::size_t i = 0; i < x.length(); i++)
	{
		if (x[i] < 'a' || x[i] > 'z')
		{
			return false;
		}
	}
	return true;
}
